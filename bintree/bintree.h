#pragma once
#include "nodes.h"

template <class T>
class bintree
{
private:
    /* data */
    BinNode<T> *root;
    int h(BinNode<T> *t);
    int max(int l, int r);
public:
    bintree(BinNode<T> *h = nullptr): root(h){};
    bintree(T k);
    int height();
    //TODO: bejárások
    /* * /
    void inOrder();
    void preOrder();
    void postOrder();
    //void levelOrder();
    virtual void insert(T k);
    virtual T remove(T k);
    virtual bool IsIn(T k);
    / * */
    ~bintree();
};

