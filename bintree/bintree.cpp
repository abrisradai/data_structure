#include "bintree.h"
template <class T>
bintree<T>::bintree(T k)
{
    BinNode<T> node(k);
    root = &node;
}
template <class T>
bintree<T>::~bintree()
{
}
template <class T>
int bintree<T>::height(){
    return this->h(root);
}
template <class T>
int bintree<T>::h(BinNode<T> *t){
    if(t != nullptr){
     return 1+this->max(this->h(t->left),this->h(t->right));
    } else {
        return -1;
    }
}
template <class T>
int bintree<T>::max(int l, int r){
    return (l > r) ? l : r;
}