#pragma once
template <class T>
class Node{
    public:
        Node(T k, Node *p = nullptr): key(k), next(p) {};
        T key;
        Node *next;
};

template <class T>
class BinNode{
    public:
        BinNode(T k, BinNode *l = nullptr, BinNode *r = nullptr): 
        key(k), left(l), right(r) {};
        T key;
        BinNode *left;
        BinNode *right;    
};